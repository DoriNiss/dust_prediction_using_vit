import torch
from torch.utils.data import Dataset
from torch.utils.data.dataloader import default_collate 
import numpy as np
from perlin_numpy import generate_perlin_noise_2d # !pip install git+https://github.com/pvigier/perlin-numpy

"""
    To be used like that:
    Inside training/validation loop: 
        for minibatch, _ in loader:
    dataloader definition:
        train_dataset = DustPredictionDataset(....),
        train_loader = DataLoader(train_dataset, batch_size=64, shuffle=True,collate_fn=dust_prediction_collate)
    augmentation_tensor size: N,self.meteorology[1:] (one tensor of both clear and event days - not from validation set!)
    Available agumentations:
        no_augmentation (default)
        augmentation_perlin
"""

class DustPredictionDataset(Dataset):
    def __init__(self, meteorology_full_tensor, dust_full_tensor, times, augmentation=None, th=73.4, dust_idx=0, 
                 importance_events_ratio=0.4, augmentation_tensor=None):
        self.meteorology = meteorology_full_tensor
        self.dust = dust_full_tensor
        self.times = times
        self.augmentation = augmentation or no_augmentation
        self.th = th
        self.labels = torch.where(dust_full_tensor[:,dust_idx]>=th, 1., 0.).to(dust_full_tensor.device) # 1 are events, 0 are clear days
        self.default_importance_events_ratio = importance_events_ratio # if set to None - will return simply the batch without duplicating by importance
        self.dust_idx = dust_idx
        self.augmentation_tensor = augmentation_tensor
        if augmentation_tensor:
            self.init_augmentation_tensors(augmentation_tensor)

    def __len__(self):
        return self.dust.shape[0]

    def __getitem__(self, idx):
        x = self.augmentation(self.meteorology[idx,:,:,:], idx)
        return x , self.dust[idx,:], self.times[idx]
        
        # if self.importance_events_ratio is None:
        #     return x , self.dust[idx,:], self.times[idx]
        # print(idx)
        # x,dust = self.sample_by_importance(x, self.dust[idx,:])
        # return x, dust, self.times[idx]
    
    def sample_by_importance(self, meteorology, dust, events_ratio=None, debug=False):
        """
            Input: meteorology - [batch_size,self.meteorology.shape[1:]]
                   dust - [batch_size,self.dust.shape[1:]]
            Output: (meteorology, dust), but with some clear days' rows replaced with duplications of dust event' rows.
            The number of replaced rows makes the ratio of events to batch_size equals to events_ratio
            If can't pupolate the correct amount of events - the output is regular (no importance sampling)
        """
        default_return = (meteorology, dust)  
        if events_ratio==-1:
            return default_return 
        r = events_ratio or self.default_importance_events_ratio
        batch_size = meteorology.shape[0]     
        final_num_events = int(r*batch_size)
        try:
            events_idxs = (dust[:,self.dust_idx]>=self.th).nonzero()[:,0]
            clear_idxs = (dust[:,self.dust_idx]<self.th).nonzero()[:,0]
            num_events_to_populate = final_num_events-events_idxs.shape[0]
            events_to_populate_from_idxs = events_idxs[torch.multinomial(events_idxs*1.,
                                                                         num_samples=num_events_to_populate,
                                                                         replacement=True)]
            clear_to_populate_idxs = clear_idxs[torch.multinomial(clear_idxs*1.,
                                                                  num_samples=num_events_to_populate,
                                                                  replacement=False)]
            meteorology_by_importance,dust_by_importance = meteorology,dust
            meteorology_by_importance[clear_to_populate_idxs] = meteorology_by_importance[events_to_populate_from_idxs]
            dust_by_importance[clear_to_populate_idxs] = dust_by_importance[events_to_populate_from_idxs]
            return meteorology_by_importance, dust_by_importance
        except Exception as exc:
            if debug:
                print("Could not perform importance sampling for this batch:")
                print(exc)
                print("dust:",dust[:,self.dust_idx],"events:",(dust[:,self.dust_idx]>=self.th).nonzero(),
                      (dust[:,self.dust_idx]>=self.th).nonzero().shape)
                print("final_num_events:",final_num_events,"num_events_to_populate = final_num_events-events_idxs.shape[0]")
            return default_return
        
    def init_augmentation_tensors(self, augmentation_tensor):
        self.augmentation_tensor_clear = augmentation_tensor[self.dust[:,self.dust_idx]<self.th]
        self.augmentation_tensor_events = augmentation_tensor[self.dust[:,self.dust_idx]>=self.th]

def dust_prediction_collate(batch):
    new_batch = []
    timestamps = []
    for _batch in batch:
        new_batch.append(_batch[:-1])
        timestamps.append(_batch[-1])
    return default_collate(new_batch), timestamps

def no_augmentation(meteorology, idx):
    return meteorology

# +
# TODO - turn augmentation into a class so all the configurations can be changed from a notebook

def augmentation_perlin(meteorology_batch, idx_batch, noise_odds=0.8, perlin_size=(3,3), perlin_amplitude=0.1):
    """
        noise_odds: there is a chance of noise_odds to add any noise (the rest will stay untouched)
    """
    def augmentation_perlin_from_noise_tensor(initial_tensor, idxs_to_change, noise_tensor, noise_odds, 
                                              debug, perlin_size, perlin_amplitude)
        _,C,H,W = initial_tensor.shape
        B = idxs_to_change.shape[0]
        N = noise_tensor.shape[0]
        num_zeros = int(N/noise_odds-N)
        noise_set_zeros_expanded = torch.cat((noise_tensor.new_zeros(num_zeros,C,H,W),noise_tensor))
        idxs_to_choose_from = torch.arange(noise_tensor.shape[0])*1.
        try:
            choosen_idxs = torch.multinomial(idxs_to_choose_from,num_samples=B,replacement=True)
            noise_set = noise_set_zeros_expanded[choosen_idxs]
            perlin_noise = generate_perlin_noise_2d((H, W), perlin_size) # one for all
            initial_tensor[idxs_to_change] = perlin_amplitude*perlin_noise+(1-perlin_amplitude)*noise_set
        except Exception as exc:
            if debug:
                print("Could not create augmented noisy tensors:")
                print(exc)
                print("idxs_to_choose_from:",idxs_to_choose_from,",num_samples:",B, 
                      "noise_set_zeros_expanded:",noise_set_zeros_expanded.shape, "idxs_to_change:",idxs_to_change,
                       idxs_to_change.shape, "initial_tensor[idxs_to_change]:",initial_tensor[idxs_to_change].shape)
        return initial_tensor        
    dust_batch = self.dust[idx_batch,:]
    try:
        idxs_to_change_clear = (dust_batch[:,self.dust_idx]>=self.th).nonzero()[:,0]
        idxs_to_change_events = (dust_batch[:,self.dust_idx]>=self.th).nonzero()[:,0]
        meteorology_batch = augmentation_perlin_from_noise_tensor(meteorology_batch, idxs_to_change_clear, 
                                                                  self.augmentation_tensor_clear, noise_odds, 
                                                                  self.debug, perlin_size, perlin_amplitude)
        meteorology_batch = augmentation_perlin_from_noise_tensor(meteorology_batch, idxs_to_change_events, 
                                                                  self.augmentation_tensor_events, noise_odds, 
                                                                  self.debug, perlin_size, perlin_amplitude)
    except Exception as exc:
        if debug:
            print("Could not create augmented noisy tensors - could not find clear or events in batch:")
            print(exc)
            print("idxs_to_change_clear:",(dust_batch[:,self.dust_idx]<self.th).nonzero(), 
                  "idxs_to_change_events:",(dust_batch[:,self.dust_idx]>=self.th).nonzero())
    return initial_tensor            
